package com.abusix.nativejvmleaksample

import sun.misc.Unsafe

fun main() {
    val f = Unsafe::class.java.getDeclaredField("theUnsafe").apply { isAccessible = true }
    val unsafe = f.get(null) as Unsafe

    val pid = ProcessHandle.current().pid()
    val runtime = Runtime.getRuntime()

    for (i in 1..1000) {
        Thread.sleep(1000)

        // allocate native memory (10 MB)
        unsafe.allocateMemory(10 * 1024 * 1024)

        // just some computations to get memory values
        val heapMB = Runtime.getRuntime().totalMemory() / 1024 / 1024

        val pr = runtime.exec(arrayOf("/bin/bash", "-c", "pmap $pid | tail -n 1 | awk '/[0-9]K/{print \$2}'"))
        pr.waitFor()
        val processKB = pr.inputStream.use { it.readAllBytes().decodeToString().trim().dropLast(1) }
        val processMB = processKB.toLong() / 1024

        println("Current heap: $heapMB MB, current process memory: $processMB MB, iteration $i of 1000")
    }

    readLine()
}