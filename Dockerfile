FROM openjdk:11-jre-slim-buster

ENV BASE_DIR=/opt/sample

RUN mkdir -p ${BASE_DIR} \
    && apt-get update -qy \
    && apt-get install -y jq procps curl unzip git autoconf gcc make graphviz \
    && git clone https://github.com/jemalloc/jemalloc.git \
    && cd jemalloc \
    && autoconf \
    && ./configure --enable-prof \
    && make && make install

# Load jemalloc into the jvm when it starts and configure it
ENV LD_PRELOAD=/usr/local/lib/libjemalloc.so
# every 2^28 (256 MB) bytes we get a new report
ENV MALLOC_CONF=prof:true,lg_prof_interval:28,lg_prof_sample:17,prof_prefix:/tmp/jeprof

WORKDIR ${BASE_DIR}

ADD target/native-jvm-leak-example-1.0-SNAPSHOT-jar-with-dependencies.jar app.jar

CMD ["/usr/local/openjdk-11/bin/java","-jar","-Xmx512M","-Xms512M","app.jar"]
